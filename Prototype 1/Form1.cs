﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;
using EHLLAPIInterface;

namespace Prototype_1
{
    public partial class Form1 : Form
    {
        DataSet mappingConfiguration = new DataSet();

        public Form1()
        {
            InitializeComponent();
            connectedBehaviour(false);
        }

        private void AppendTextToRichTextBox1(RichTextBox RTB, string line)
        {
            if (RTB.InvokeRequired)
                RTB.Invoke(new Action(() => {
                    RTB.AppendText(line + Environment.NewLine);
                    RTB.ScrollToCaret();
                }));
            else
                RTB.AppendText(line + Environment.NewLine);
        }

        void outToLog(string output)
        {
            output = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + "    " + output;
            AppendTextToRichTextBox1(richTextBox1, output);
        }

        void connectedBehaviour(bool connect)
        {
            buttonConnect.Enabled = !connect;
            textSessionid.Enabled = !connect;
            buttonDisconnect.Enabled = connect;
            buttonStart.Enabled = connect;
        }



        private void buttonConnect_Click(object sender, EventArgs e)
        {
            var rtnMsg = EHLLAPIWrapper.Connect(textSessionid.Text).ToString();
            switch (rtnMsg)
            {
                case "0":
                    rtnMsg = "Connected to session " + textSessionid.Text;
                    connectedBehaviour(true);
                    break;
                case "1":
                    rtnMsg = "Can't connect to session " + textSessionid.Text;
                    break;
                case "11":
                    rtnMsg = "Session " + textSessionid.Text + " is in use";
                    break;
                default:
                    rtnMsg = "Unknown return message ";
                    break;
            }
            outToLog(rtnMsg);
        }

        private void buttonDisconnect_Click(object sender, EventArgs e)
        {
            var rtnMsg = EHLLAPIWrapper.Disconnect(textSessionid.Text).ToString();
            switch (rtnMsg)
            {
                case "0":
                    rtnMsg = "Disconnected from session " + textSessionid.Text;
                    connectedBehaviour(false);
                    break;
                case "1":
                    rtnMsg = "Failed to disconnnect from session " + textSessionid.Text;
                    break;
                default:
                    rtnMsg = "Unknown return message ";
                    break;
            }
            outToLog(rtnMsg);
        }

        private void buttonStart_Click(object sender, EventArgs e)
        {
            Task t = new Task(() =>
            {
                //int p;
                //EHLLAPIWrapper.GetCursorPos(out p);
                //outToLog("cursor position " + p.ToString());
                //EHLLAPIWrapper.SetCursorPos(453);
                //EHLLAPIWrapper.SendStr("dede");
                //EHLLAPIWrapper.SetCursorPos(453 + 80);
                //EHLLAPIWrapper.SendStr("");


                EHLLAPIWrapper.SetCursorPos(1527);
                EHLLAPIWrapper.SendStr("addlible tmstrain");
                EHLLAPIWrapper.SendStr("@E");

                Thread.Sleep(500);

                string exceptMessage;
                EHLLAPIWrapper.ReadScreen(Convert.ToInt32((23 * 80) + 2), Convert.ToInt32(40), out exceptMessage);
                if (exceptMessage != "") outToLog(exceptMessage);

                EHLLAPIWrapper.SetCursorPos(1527);
                EHLLAPIWrapper.SendStr("@A@E");

                outToLog("Call Program TAB015");
                EHLLAPIWrapper.SendStr("call tab015");
                EHLLAPIWrapper.SendStr("@E");

                Thread.Sleep(500);

                string noAcc = "0050005020";
                outToLog("Find Account no " + noAcc);
                EHLLAPIWrapper.SetCursorPos((80 * 5) + 67);
                EHLLAPIWrapper.SendStr(noAcc);
                EHLLAPIWrapper.SendStr("@E");

                Thread.Sleep(500);

                outToLog("Display detail information account  " + noAcc);
                EHLLAPIWrapper.SetCursorPos((80 * 9) + 3);
                EHLLAPIWrapper.SendStr("5");
                EHLLAPIWrapper.SendStr("@E");

                Thread.Sleep(1000);

                outToLog("Exit display detail information account  " + noAcc);
                EHLLAPIWrapper.SendStr("@c");

                Account acc = new Account();
                acc.AccountNo = "0060005000";
                acc.AccountName = "Satria Langit";
                acc.ShortName = "Satria";
                acc.ReligionCode = "04";
                acc.Address1 = "Jl Angkasa No 90";
                acc.PostalCode = "50001";
                acc.Gender = "M";
                acc.Handphone = "0819999999";
                acc.ProfesionCode = "DOC";
                acc.AmountBalance = "10000000";

                outToLog("Create new account no " + acc.AccountNo);
                EHLLAPIWrapper.SendStr("@6");

                Thread.Sleep(500);
                EHLLAPIWrapper.SetCursorPos((80 * 8) + 34);
                EHLLAPIWrapper.SendStr("@F");
                EHLLAPIWrapper.SendStr(acc.AccountNo);

                EHLLAPIWrapper.SetCursorPos((80 * 9) + 34);
                EHLLAPIWrapper.SendStr("@F");
                EHLLAPIWrapper.SendStr(acc.AccountName);

                EHLLAPIWrapper.SetCursorPos((80 * 10) + 34);
                EHLLAPIWrapper.SendStr("@F");
                EHLLAPIWrapper.SendStr(acc.ShortName);

                EHLLAPIWrapper.SetCursorPos((80 * 11) + 34);
                EHLLAPIWrapper.SendStr("@F");
                EHLLAPIWrapper.SendStr(acc.ReligionCode);

                EHLLAPIWrapper.SetCursorPos((80 * 12) + 34);
                EHLLAPIWrapper.SendStr("@F");
                EHLLAPIWrapper.SendStr(acc.Address1);

                EHLLAPIWrapper.SetCursorPos((80 * 15) + 34);
                EHLLAPIWrapper.SendStr("@F");
                EHLLAPIWrapper.SendStr(acc.PostalCode);

                EHLLAPIWrapper.SetCursorPos((80 * 16) + 34);
                EHLLAPIWrapper.SendStr("@F");
                EHLLAPIWrapper.SendStr(acc.Gender);

                EHLLAPIWrapper.SetCursorPos((80 * 17) + 34);
                EHLLAPIWrapper.SendStr("@F");
                EHLLAPIWrapper.SendStr(acc.Handphone);

                EHLLAPIWrapper.SetCursorPos((80 * 18) + 34);
                EHLLAPIWrapper.SendStr("@F");
                EHLLAPIWrapper.SendStr(acc.ProfesionCode);

                EHLLAPIWrapper.SetCursorPos((80 * 19) + 34);
                EHLLAPIWrapper.SendStr("@F");
                EHLLAPIWrapper.SendStr(acc.AmountBalance);

                EHLLAPIWrapper.SetCursorPos((80 * 21) + 43);
                EHLLAPIWrapper.SendStr("Y");

                outToLog("Ready to execute inputed account " + acc.AccountNo);
                
                outToLog("Executed in 5");
                Thread.Sleep(1000);
                outToLog("Executed in 4");
                Thread.Sleep(1000);
                outToLog("Executed in 3");
                Thread.Sleep(1000);
                outToLog("Executed in 2");
                Thread.Sleep(1000);
                outToLog("Executed in 1");
                Thread.Sleep(1000);

                EHLLAPIWrapper.SendStr("@E");

            });

            t.Start();
            
        }

    }

    public class Account
    {
        public string AccountNo { get; set; }
        public string AccountName { get; set; }
        public string ShortName { get; set; }
        public string ReligionCode { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string PostalCode { get; set; }
        public string Gender { get; set; }
        public string Handphone { get; set; }
        public string ProfesionCode { get; set; }
        public string AmountBalance { get; set; }
    }
}
