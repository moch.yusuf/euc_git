﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;
using EHLLAPIInterface;
using ADOX;
using System.Xml.Serialization;

namespace Prototype2
{
    public partial class Form1 : Form
    {
        public static string SelectedTable = string.Empty;
        public static DataTable dataTable = null;
        public static Configuration configuration = null;

        public Form1()
        {
            InitializeComponent();
        }

        private void AppendTextToRichTextBox1(RichTextBox RTB, string line)
        {
            if (RTB.InvokeRequired)
                RTB.Invoke(new Action(() => {
                    RTB.AppendText(line + Environment.NewLine);
                    RTB.ScrollToCaret();
                }));
            else
                RTB.AppendText(line + Environment.NewLine);
        }

        void outToLog(string output)
        {
            output = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + "    " + output;
            AppendTextToRichTextBox1(richTextBox1, output);
        }

        void connectedBehaviour(bool connect)
        {
            buttonConnect.Enabled = !connect;
            textSessionid.Enabled = !connect;
            txtConfiguration.Enabled = !connect;
            btnBrowserConf.Enabled = !connect;
            btnBrowseExcel.Enabled = !connect;
            txtExcelTemplate.Enabled = !connect;
            buttonDisconnect.Enabled = connect;
            buttonStart.Enabled = connect;
        }

        private void buttonConnect_Click(object sender, EventArgs e)
        {
            var rtnMsg = EHLLAPIWrapper.Connect(textSessionid.Text).ToString();
            switch (rtnMsg)
            {
                case "0":
                    rtnMsg = "Connected to session " + textSessionid.Text;
                    connectedBehaviour(true);
                    break;
                case "1":
                    rtnMsg = "Can't connect to session " + textSessionid.Text;
                    break;
                case "11":
                    rtnMsg = "Session " + textSessionid.Text + " is in use";
                    break;
                default:
                    rtnMsg = "Unknown return message ";
                    break;
            }
            outToLog(rtnMsg);
        }

        private void buttonDisconnect_Click(object sender, EventArgs e)
        {
            var rtnMsg = EHLLAPIWrapper.Disconnect(textSessionid.Text).ToString();
            switch (rtnMsg)
            {
                case "0":
                    rtnMsg = "Disconnected from session " + textSessionid.Text;
                    connectedBehaviour(false);
                    break;
                case "1":
                    rtnMsg = "Failed to disconnnect from session " + textSessionid.Text;
                    break;
                default:
                    rtnMsg = "Unknown return message ";
                    break;
            }
            outToLog(rtnMsg);
        }

        private void btnBrowseExcel_Click(object sender, EventArgs e)
        {
            OpenFileDialog fdlg = new OpenFileDialog();
            fdlg.Title = "Select file";
            fdlg.InitialDirectory = @"c:\";
            fdlg.FileName = txtExcelTemplate.Text;
            fdlg.Filter = "Excel Sheet(*.xls)|*.xls|All Files(*.*)|*.*";
            fdlg.FilterIndex = 1;
            fdlg.RestoreDirectory = true;
            if (fdlg.ShowDialog() == DialogResult.OK)
            {
                txtExcelTemplate.Text = fdlg.FileName;
                Import();
                Application.DoEvents();
            }
        }

        private void Import()
        {
            if (txtExcelTemplate.Text.Trim() != string.Empty)
            {
                try
                {
                    string[] strTables = GetTableExcel(txtExcelTemplate.Text);
                    dataTable = null;
                    SelectSheet objSelectTable = new SelectSheet(strTables);
                    objSelectTable.ShowDialog(this);
                    objSelectTable.Dispose();
                    if ((SelectedTable != string.Empty) && (SelectedTable != null))
                    {
                        DataTable dt = GetDataTableExcel(txtExcelTemplate.Text, SelectedTable);
                        dataTable = dt;
                        dataGridView1.DataSource = dataTable.DefaultView;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message.ToString());
                }
            }
        }

        public static DataTable GetDataTableExcel(string strFileName, string Table)
        {
            System.Data.OleDb.OleDbConnection conn = new System.Data.OleDb.OleDbConnection("Provider=Microsoft.Jet.OleDb.4.0; Data Source = " + strFileName + "; Extended Properties = \"Excel 8.0;HDR=Yes;IMEX=1\";");
            conn.Open();
            string strQuery = "SELECT * FROM [" + Table + "]";
            System.Data.OleDb.OleDbDataAdapter adapter = new System.Data.OleDb.OleDbDataAdapter(strQuery, conn);
            System.Data.DataSet ds = new System.Data.DataSet();
            adapter.Fill(ds);
            conn.Close();
            return ds.Tables[0];
        }

        public static string[] GetTableExcel(string strFileName)
        {
            string[] strTables = new string[100];
            Catalog oCatlog = new Catalog();
            ADOX.Table oTable = new ADOX.Table();
            ADODB.Connection oConn = new ADODB.Connection();
            oConn.Open("Provider=Microsoft.Jet.OleDb.4.0; Data Source = " + strFileName + "; Extended Properties = \"Excel 8.0;HDR=Yes;IMEX=1\";", "", "", 0);

            oCatlog.ActiveConnection = oConn;
            if (oCatlog.Tables.Count > 0)
            {
                int item = 0;
                foreach (ADOX.Table tab in oCatlog.Tables)
                {
                    if (tab.Type == "TABLE")
                    {
                        strTables[item] = tab.Name;
                        item++;
                    }
                }
            }
            oConn.Close();
            return strTables;
        }




        private void button1_Click(object sender, EventArgs e)
        {
            Configuration config = new Prototype2.Configuration();
            config.ScreenSize = 132;

            // Column 1 
            Event col = new Event();
            col.Sequence = 1;
            col.Name = "AccountNo";
            col.ScreenId = "";
            col.CursorPosition = 233;
            col.Action.Type = "GET";

            config.Events.Add(col);

            // Column 2
            Event col2 = new Event();
            col2.Sequence = 2;
            col2.Name = "TDNo";
            col2.ScreenId = "";
            col2.CursorPosition = 233;
            col2.Action.Type = "INPUT";

            config.Events.Add(col2);


            System.Xml.Serialization.XmlSerializer writer =
            new System.Xml.Serialization.XmlSerializer(typeof(Configuration));

            var confDir = "Configuration";
            var path = $"{confDir}//TDRate.xml";
            if (!System.IO.Directory.Exists(confDir))
            {
                System.IO.Directory.CreateDirectory(confDir);
            }
            System.IO.FileStream file = System.IO.File.Create(path);

            writer.Serialize(file, config);
            file.Close();


        }

        private void btnBrowserConf_Click(object sender, EventArgs e)
        {
            OpenFileDialog fdlg = new OpenFileDialog();
            fdlg.Title = "Select file";
            fdlg.FileName = txtConfiguration.Text;
            fdlg.Filter = "Configuration(*.xml)|*.xml|All Files(*.*)|*.*";
            fdlg.FilterIndex = 1;
            fdlg.RestoreDirectory = true;
            if (fdlg.ShowDialog() == DialogResult.OK)
            {
                txtConfiguration.Text = fdlg.FileName;
                ReadConfiguration();
                Application.DoEvents();
            }
        }

        private Boolean ReadConfiguration()
        {
            if (txtConfiguration.Text.Trim() != string.Empty)
            {
                try
                {
                    XmlSerializer confSerializer = new XmlSerializer(typeof(Configuration));
                    using (System.IO.FileStream fs = System.IO.File.OpenRead(txtConfiguration.Text.Trim()))
                    {
                        configuration = (Configuration)confSerializer.Deserialize(fs);
                        return true;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message.ToString());
                    return false;
                }
            }
            return false;
        }

        private void btnCekConfiguration_Click(object sender, EventArgs e)
        {
            if (ReadConfiguration() == true)
            {
                MessageBox.Show("Valid Configuration");
            }
        }

        private void buttonStart_Click(object sender, EventArgs e)
        {
            if (configuration == null)
            {
                MessageBox.Show("Select configuration");
            }
            if (dataTable == null)
            {
                MessageBox.Show("Select excel template to process");
            }
            process();
        }

        private void process()
        {

            Task t = new Task(() =>
            {
                int sleep = 200;
                int counter = 1;
                foreach (DataRow row in dataTable.Rows)
                {
                    outToLog($"Execute row no {counter.ToString()}");
                    foreach (Event col in configuration.Events)
                    {
                        int screenSize = configuration.ScreenSize;
                        // Check Screen Id is match
                        string screenId = ReadScreenId(col.CursorPosition, col.ScreenId.Length, col.ScreenId, sleep);
                        if (screenId == col.ScreenId)
                        {
                            if (col.Action.Type == "GET")
                            {
                                if (col.Action.Get.Type == "SUBFILE")
                                {
                                    // Loop through subfiles
                                    bool eof = false;
                                    bool found = false;
                                    while (eof == false)
                                    {
                                        for (int i = 0; i < col.Action.Get.Subfile.PageSize; i++)
                                        {
                                            string value = "";
                                            int startCursor = (col.Action.Get.Subfile.PosisiRow * screenSize) + col.Action.Get.Subfile.PosisiColumn + (i * screenSize);
                                            EHLLAPIWrapper.ReadScreen(Convert.ToInt32(startCursor), Convert.ToInt32(col.Action.Get.Subfile.FieldLength), out value);
                                            //Thread.Sleep(100);
                                            if (row[col.Name].ToString() == value)
                                            {
                                                found = true;
                                                outToLog($"Value {row[col.Name].ToString()} found");
                                                int optionCursor = (col.Action.Get.Subfile.PosisiRow * screenSize) + col.Action.Get.Subfile.FoundAction.OptionPosition + (i * screenSize);
                                                EHLLAPIWrapper.SetCursorPos(optionCursor);
                                                EHLLAPIWrapper.SendStr(col.Action.Get.Subfile.FoundAction.Option);
                                                sendCommand("@E");
                                                break; // out from for loop
                                            }
                                        }

                                        if (found == true) break; // out from while loop

                                        // check is end of file
                                        string message = "";
                                        EHLLAPIWrapper.ReadScreen(Convert.ToInt32(col.Action.Get.Subfile.EofCursorPosition), Convert.ToInt32(col.Action.Get.Subfile.EofMessage.Length), out message);
                                        //Thread.Sleep(100);
                                        if (message == col.Action.Get.Subfile.EofMessage)
                                        {
                                            eof = true;
                                            outToLog($"end of file reached");
                                        }
                                        if (eof == false) sendCommand("@v"); // PageDown
                                    }
                                    if (found == false) sendCommand(col.Action.Get.Subfile.NotFoundAPIKey);
                                }
                                else if (col.Action.Get.Type == "FORM")
                                {
                                    // Get value from screen
                                    string value = "";
                                    EHLLAPIWrapper.SetCursorPos(0);
                                    EHLLAPIWrapper.ReadScreen(Convert.ToInt32(col.Action.Get.Form.CursorPosition), Convert.ToInt32(col.Action.Get.Form.FieldLength), out value);
                                    Thread.Sleep(sleep);
                                    if (row[col.Name].ToString() == value)
                                    {
                                        sendCommand(col.Action.Get.Form.FoundAPIKey);
                                    } else
                                    {
                                        sendCommand(col.Action.Get.Form.NotFoundAPIKey);
                                        outToLog($"Value {row[col.Name].ToString()} does not match with screen value, screen value is {value}");
                                        break; // Break from configuration column loop
                                    }
                                }
                            }
                            else if (col.Action.Type == "INPUT")
                            {
                                EHLLAPIWrapper.SetCursorPos(Convert.ToInt32(col.Action.Input.CursorPosition));
                                EHLLAPIWrapper.SendStr("@F");
                                EHLLAPIWrapper.SendStr(row[col.Name].ToString());
                                Thread.Sleep(sleep);

                                sendCommand(col.Action.Input.NextAPIKey);
                                   
                            }
                            else if (col.Action.Type == "COPY")
                            {
                                string value = "";
                                EHLLAPIWrapper.ReadScreen(Convert.ToInt32(col.Action.Copy.FromCursorPosition), Convert.ToInt32(col.Action.Copy.FieldLength), out value);
                                Thread.Sleep(sleep);

                                sendCommand(col.Action.Copy.AfterReadAPIKey);

                                EHLLAPIWrapper.SetCursorPos(Convert.ToInt32(col.Action.Copy.ToCursorPosition));
                                EHLLAPIWrapper.SendStr("@F");
                                EHLLAPIWrapper.SendStr(value);
                                Thread.Sleep(sleep);

                                sendCommand(col.Action.Copy.NextAPIKey);

                            }
                            else if (col.Action.Type == "API")
                            {
                                sendCommand(col.Action.APIKey);   
                            }
                        }
                        else
                        {
                            outToLog($"Screen Id {col.ScreenId} does not match with screen");
                        }
                    }
                    counter++;
                }
            });

            t.Start();
        }

        private void sendCommand(string apiKey)
        {
            if (!String.IsNullOrEmpty(apiKey))
            {
                EHLLAPIWrapper.SendStr(apiKey);
                Thread.Sleep(500);
            }
        }

        private string ReadScreenId(int cursor, int length, string screenId, int sleep)
        {
            int retry = 0;
            string screen = "";
            while (retry < 3 && screen != screenId)
            {
                EHLLAPIWrapper.SetCursorPos(0);
                EHLLAPIWrapper.ReadScreen(Convert.ToInt32(cursor), Convert.ToInt32(length), out screen);
                Thread.Sleep(sleep);
                retry++;
            }
            return screen;
        }

        
    }
}
