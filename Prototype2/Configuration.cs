﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Prototype2
{
    public class Configuration
    {
        public Configuration()
        {
            this.Events = new List<Event>();
        }
        public int ScreenSize { get; set; }
        public List<Event> Events { get; set; }
    }

    public class Event
    {
        public Event()
        {
            this.Action = new Prototype2.Act();
        }
        public int Sequence { get; set; }
        public string Name { get; set; }
        public string ScreenId { get; set; }
        public int CursorPosition { get; set; }
        public Act Action { get; set; }
    }

    public class Act
    {
        public Act()
        {
            this.Input = new ActionInput();
            this.Get = new ActionGet();
        }
        // Type 
        // GET -> To comparing value between as400 and excel column value
        // INPUT -> To send excel column value into as400
        // API -> To send API Key to as400
        // COPY -> Get Value from screen and paste it to screen
        public string Type { get; set; } 
        public ActionInput Input { get; set; }
        public ActionGet Get { get; set; }
        public ActionCopy Copy { get; set; }
        public string APIKey { get; set; }
    }

    public class ActionGet
    {
        public ActionGet()
        {
            this.Form = new Prototype2.FormGet();
            this.Subfile = new Prototype2.SubfileGet();
        }
        // Type
        // FORM : compare value from form screen type
        // SUBFILE : compare value from loop through subfile records
        public string Type { get; set; }
        public FormGet Form { get; set; }
        public SubfileGet Subfile { get; set; }
        
    }

    public class ActionInput
    {
        public int CursorPosition { get; set; }
        public int FieldLength { get; set; }
        public string NextAPIKey { get; set; }
    }

    public class FormGet
    {
        public int CursorPosition { get; set; }
        public int FieldLength { get; set; }
        public string NotFoundAPIKey { get; set; }
        public string FoundAPIKey { get; set; }
    }

    public class SubfileGet
    {
        public int PosisiColumn { get; set; }
        public int PosisiRow { get; set; }
        public int PageSize { get; set; }
        public int FieldLength { get; set; }
        public int EofCursorPosition { get; set; }
        public string EofMessage { get; set; }
        public string NotFoundAPIKey { get; set; }
        public SubfileAction FoundAction { get; set; }
    }

    public class SubfileAction
    {
        public int OptionPosition { get; set; }
        public string Option { get; set; }
    }

    public class ActionCopy
    {
        public int FromCursorPosition { get; set; }
        public int ToCursorPosition { get; set; }
        public int FieldLength { get; set; }
        public string AfterReadAPIKey { get; set; }
        public string NextAPIKey { get; set; }
    }


}
